package com.groundgurus.oop;

public class Shop {
    private String name;
    private String[] items;
    private int zipCode;
    private String type;

    public Shop(String name) {
        this.name = name;
    }

    public Shop(String name, String[] items, int zipCode, String type) {
        this.name = name;
        this.items = items;
        this.zipCode = zipCode;
        this.type = type;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        if (zipCode > 0) {
            this.zipCode = zipCode;
        } else {
            System.out.println("Zip code is invalid");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getItems() {
        return items;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void printDetails() {
        System.out.println("Name: " + name);

        System.out.print("Items: ");
        for (int i = 0; i < items.length; i++) {
            System.out.print(items[i]);

            if (i < items.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();

        System.out.println("Zip Code: " + zipCode);
        System.out.println("Type: " + type);
    }
}
