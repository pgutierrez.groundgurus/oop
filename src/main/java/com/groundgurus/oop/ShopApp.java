package com.groundgurus.oop;

public class ShopApp {
    public static void main(String[] args) {
        WineShop myShop1 = new WineShop("Ministop",
                new String[] {
                        "Mineral Water 500ml",
                        "C2 500ml"
                }, 1603, "24/7", false);
        myShop1.printDetails();
        //myShop1.isAreMinorsAllowed();

        //myShop1.name = "Ministop";
        //myShop1.zipCode = -1234;
        //myShop1.setZipCode(-1234);

        System.out.println("----------------------");

        CakeShop myShop2 = new CakeShop("Red Ribbon",
                new String[] {
                        "Cake A",
                        "Cake B"
                }, 2000, "10am-10pm");
        myShop2.printDetails();

        VacantLot myVacantLot = new VacantLot();
        myVacantLot.addShop(myShop1);
        myVacantLot.addShop(myShop2);

        //myShop2.setZipCode(2000);
        //myShop2.name = "7Eleven";

        //System.out.println(myShop1.name);
        //System.out.println(myShop1.getZipCode());
        //System.out.println(myShop2.name);
        //System.out.println(myShop2.getZipCode());
    }
}
