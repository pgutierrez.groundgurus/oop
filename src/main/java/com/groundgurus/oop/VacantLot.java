package com.groundgurus.oop;

import java.util.ArrayList;
import java.util.List;

public class VacantLot {
    private List<Shop> shops = new ArrayList<>();

    public void addShop(Shop myShop) {
        if (myShop instanceof WineShop) {
            System.out.println("Get more requirements from barangay");
            //shops.add(myShop);
        } else if (myShop instanceof CakeShop) {
            System.out.println("Do more things to prepare");
            //shops.add(myShop);
        }

        shops.add(myShop);
    }
//
//    public void addShop(WineShop myShop) {
//        System.out.println("Get more requirements from barangay");
//        shops.add(myShop);
//    }
//
//    public void addShop(CakeShop cakeShop) {
//        System.out.println("Do more things to prepare");
//        shops.add(cakeShop);
//    }

    public List<Shop> getShops() {
        return shops;
    }

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }
}
