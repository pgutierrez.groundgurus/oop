package com.groundgurus.oop;

public class WineShop extends Shop {
    private boolean areMinorsAllowed;

    public WineShop(String name) {
        super(name);
    }

    public WineShop(String name, String[] items, int zipCode, String type) {
        super(name, items, zipCode, type);
    }

    public WineShop(String name, String[] items, int zipCode, String type, boolean areMinorsAllowed) {
        super(name, items, zipCode, type);
        this.areMinorsAllowed = areMinorsAllowed;
    }

    public boolean isAreMinorsAllowed() {
        return areMinorsAllowed;
    }

    public void setAreMinorsAllowed(boolean areMinorsAllowed) {
        this.areMinorsAllowed = areMinorsAllowed;
    }

    @Override
    public void printDetails() {
        super.printDetails();
        System.out.println("Are Minors Allowed? " + areMinorsAllowed);
    }
}
