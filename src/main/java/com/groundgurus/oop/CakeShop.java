package com.groundgurus.oop;

public class CakeShop extends Shop {
    public CakeShop(String name) {
        super(name);
    }

    public CakeShop(String name, String[] items, int zipCode, String type) {
        super(name, items, zipCode, type);
    }
}
